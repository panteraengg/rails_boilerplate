class ErrorsController < ApplicationController
  def not_found
    render not_found
  end

  def internal_server_error
    render internal_server_error
  end

  protected

  def not_found
    render status: :not_found, json: {errors: [I18n.t('error.not_found')]}
  end

  def internal_server_error
    render status: :internal_server_error, json: {errors: [I18n.t('error.internal_server_error')]}
  end
end
